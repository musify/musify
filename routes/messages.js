"application/javascript";
const express = require('express');
const expressValidator = require('express-validator');
const flash = require('connect-flash');
const session = require('express-session');
const router = express.Router();
const multer  = require('multer');
var moment = require('moment');
var socketio = require('socket.io');
var http = require('http');


let Message = require('../models/message');
const User = require('../models/user');






// Express Session Middleware
router.use(session({
  secret: 'keyboard cat',
  resave: true,
  saveUninitialized: true
}));


// Express Messages Middleware
router.use(require('connect-flash')());
router.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});



// Express Validator
router.use(expressValidator({
  errorFormatter: function(param, msg, value) {
      var namespace = param.split('.')
      , root    = namespace.shift()
      , formParam = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));

// AUTHENTICATION
function ensureAuthenticated(req,res,next){
  if(req.isAuthenticated()) {
    return next();
  } else {
    req.flash('danger', 'Zaloguj się');
    res.redirect('/users/login')
  }
}

//Messanger ROUTE


router.get('/:id', ensureAuthenticated, function(req,res){
  var recip = req.params.id;
  var p1 = req.params.id;
  var p2 = req.user._id;
    var p2_name = req.user.name;

User.findById(p1, function(err, person1){


      console.log(person1.name);



      console.log('p1', req.params.id);
      console.log('p2', req.user._id);
      //Check if conv for p1 exists
      Message.findOne({'person':req.params.id}, function(err, result){
        if(result){
          //Check if conv for p2 exists
          Message.findOne({'person':req.user._id}, function(err, data){
            if(data){
              console.log("konwersacja istnieje");
                res.redirect('/inbox/mail/'+ data.id);
            } else {
              User.findByIdAndUpdate(
                p1,
                {$push: {"conversation":{sender:p2, author:p2_name}}},
                {safe: true, upsert: true},
                function(err, model) {
                  if(err){ console.log(err);}
                });
                User.findByIdAndUpdate(
                  p2,
                  {$push: {"conversation":{sender:p1, author:person1.name}}},
                  {safe: true, upsert: true},
                  function(err, model) {
                    if(err){ console.log(err);}
                  });

              let conv = new Message({
                person: [req.params.id, req.user._id]
              });

              // Saving new conversation and redirecting to the room
              conv.save(function(err, room){
                if(err){res.redirect('/home')}
                else{
                  res.redirect('/inbox/mail/'+ room.id);
                }
              });
              console.log('utworzono');
            }
          });
        }    else {
          User.findByIdAndUpdate(
            p1,
            {$push: {"conversation":{sender:p2, author:p2_name}}},
            {safe: true, upsert: true},
            function(err, model) {
              if(err){ console.log(err);}
            });
            User.findByIdAndUpdate(
              p2,
              {$push: {"conversation":{sender:p1, author:person1.name}}},
              {safe: true, upsert: true},
              function(err, model) {
                if(err){ console.log(err);}
              });
            let conv = new Message({
            person:[req.params.id, req.user._id]
          });
          // Saving new conversation and redirecting to the room
          conv.save(function(err, room){
            if(err){res.redirect('/home')}
            else{
              res.redirect('/inbox/mail/'+ room.id);
            }
          });
          console.log('utworzono');

        };
      });
    });
  });

router.get('/mail/:id', ensureAuthenticated, function(req,res){
  Message.findById(req.params.id, function(err, data){
    res.render('messages',{
      msg:data,
      moment:moment
    });
  });
});

router.post('/send/:id', ensureAuthenticated, function(req,res){
  var content = req.body.content;
  var author = req.user.name;
Message.findById(req.params.id, function(err, msg_data){
  req.checkBody('content', 'Pusta wiadomość').notEmpty();
  let errors = req.validationErrors();
  if(errors){
    res.redirect('/inbox/mail/'+ req.params.id);
  } else {
  Message.findByIdAndUpdate(
    req.params.id,
    {$push: {"messages": {author: author, message: content}}},
    {safe: true, upsert: true},
    function(err, model) {
      if(err){ console.log(err);}

    });
  User.findByIdAndUpdate(
      msg_data.person[0],
      {$set: {"conversation": {last: content}}},
      {safe: true, upsert: true},
      function(err, model) {
        if(err){ console.log(err);}

      });
    User.findByIdAndUpdate(
        msg_data.person[1],
        {$set: {"conversation": {last: content}}},
        {safe: true, upsert: true},
        function(err, model) {
          if(err){ console.log(err);}
            res.redirect('/inbox/mail/' + req.params.id);
        });
  }
});
});
router.get('/', ensureAuthenticated, function(req,res){
  User.findById(req.user.id, function(err, data){
    res.render('inbox',{
      moment:moment,
      users:data
    });
  });
});
module.exports = router;
