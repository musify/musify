const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const expressValidator = require('express-validator');
const flash = require('connect-flash');
const passport = require('passport');
var randomstring = require("randomstring");
const nodemailer = require('nodemailer');


// EMAIL TRANSPORTER
let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // secure:true for port 465, secure:false for port 587
    auth: {
        user: 'bojczukpraca@gmail.com',
        pass: 'Lubieplacki1'
    }
});




// Express Messages Middleware
router.use(require('connect-flash')());
router.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});

// Express Validator
router.use(expressValidator({
  errorFormatter: function(param, msg, value) {
      var namespace = param.split('.')
      , root    = namespace.shift()
      , formParam = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));

//INCLUDE USER model

let User = require('../models/user');

router.get('/register', function(req,res){
  res.render('register');
});

// REGISTRATION
router.post('/register', function(req,res){
  const name = req.body.name;
  const username = req.body.username;
  const email = req.body.email;
  const password = req.body.password;
  const password1 = req.body.password1;
  console.log(email);
  req.checkBody('name', 'Podaj Imię').notEmpty();
  req.checkBody('email', 'Nieprawidłowy Adres Email').isEmail();
  req.checkBody('username', 'Podaj nazwę Użytkownika').notEmpty();
  req.checkBody('email', 'Podaj Email').notEmpty();
  req.checkBody('password', 'Podaj Hasło').notEmpty();
  req.checkBody('password1', 'Hasła się nie zgadzają').equals(req.body.password);

  let errors = req.validationErrors();
  // console.log(alerts, alerts1, email, username);
  if(errors){
    res.render('register',{
      errors:errors,
    });
  } else {
    // Checking if user is uniqe
    User.findOne({username:username}, function(err,user){
      if(err){
        res.redirect('/');
      };
      if(user){
        req.flash('success', 'Nazwa użytkownika zajęta');
        res.render('register');
      };
      if(!user){
        // checking if email is unique
        User.findOne({email:email}, function(err, data){
          if(err){
            res.render('/');
          };
          if(data){
            req.flash('success', 'Adres email w użyciu');
            res.render('register');
          };
          if(!data){
            // EVERYTHING CLEAR, LETS REGISTER
            let token = randomstring.generate();

            let newUser = new User({
              name:name,
              email:email,
              token:token,
              username:username,
              password:password
            });
            bcrypt.genSalt(10,function(err, salt){
              bcrypt.hash(newUser.password, salt, function(err, hash){
                if(err){
                  console.log('error');
                  req.flash('success', 'error');
                }
                newUser.password = hash;
                newUser.save(function(err){
                  if(err){
                    console.log(err);
                    req.flash('success', 'error');
                    return;
                  } else {
                    let mailOptions = {
              from:'validate@musify.com', // sender address
              to: email, // list of receivers
              subject: 'Rejestracja w Musify', // Subject line
              html:   "Witamy w Musify</br> Aby zweryfikować swój adres email kliknij poniższy link </br>  http://bojczuk.tk:3000/users/verify/" + token , // plain text body
            };

            transporter.sendMail(mailOptions, (error, info) => {
              if (error) {
                  return console.log(error);
                }
                console.log('Message %s sent: %s', info.messageId, info.response);
              });
                    req.flash('success', 'Zarejestrowano, potwierdź adres email');
                    res.redirect('/users/login')
                  }
                });
              });
            });
          }
        });
      }

    });

  }
});

//VERIFICATION ROUTE
router.get('/verify/:id', function(req,res){
    User.findOneAndUpdate({token:req.params.id}, {$set: { isVerified: true }}, function(err, user) {
      if(user){

        console.log('verified');
        res.redirect('/users/login');
      } else {
        console.log('user nie istnieje');
        res.redirect('/users/register');
      }
    });
});

// Check if user exists
router.get('/usercheck', function(req, res) {
    User.findOne({username: req.query.username}, function(err, user){
        if(err) {
          console.log(err);
        }
        var message;
        if(user) {
            message = "user exists";
        } else {
            message= "user doesn't exist";
        }
        res.json({message: message});

    });
});

//check if email exists
// Check if user exists
router.get('/emailcheck', function(req, res) {
    User.findOne({email: req.query.email}, function(err, email){
        if(err) {
          console.log(err);
        }
        var message;
        if(email) {

            message = "email exists";
        } else {
            message= "email doesn't exist";
        }
        res.json({message: message});

    });
});

// LOGIN FORM
router.get('/login', function(req,res){
  res.render('login');
});


// LOGIN PROCESS
router.post('/login', function(req,res, next){
  passport.authenticate('local',{
    successRedirect:'/home',
    failureRedirect:'/users/login',
    failureFlash:true
  })(req,res,next);
});

// logout
router.get('/logout', function(req,res){
  req.logout();
  req.flash('success', 'Wylogowano');
  res.redirect('/users/login');
});
module.exports = router;
