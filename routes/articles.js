"application/javascript";
const express = require('express');
const expressValidator = require('express-validator');
const flash = require('connect-flash');
const session = require('express-session');
const router = express.Router();
const multer  = require('multer');
const Loki = require('lokijs');
const cors = require('cors');
const fs = require('fs');
var path = require('path');
var moment = require('moment');
var io = require('../index');
var connections = [];
// io.sockets.on('connection',function(socket){
//   connections.push(socket);
//   console.log('COnnected: %s som', connections.length);
//
//   // Disconnect
//   socket.on('disconnect', function(data){
//     connections.splice(connections.indexOf(socket),1);
//     console.log('Disconnected zostalao %s', connections.length );
//
//   });
//
//   //send messages
//   socket.on('send message', function(req){
//     console.log(data);
//     io.sockets.emit('new message',{msg:req.user} );
//   });
// });


// Express Session Middleware
router.use(session({
  secret: 'keyboard cat',
  resave: true,
  saveUninitialized: true
}));


// Express Messages Middleware
router.use(require('connect-flash')());
router.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});


// models
let Article = require('../models/article');
let User = require('../models/user');

//Add ROUTE
router.get('/add', ensureAuthenticated, function(req,res){
  res.render('add', {
    title:'Add Article'
  });
});

// HOME ROUTE
router.get('/home', function(req,res){
  Article.find({}, function(err, articles){
    User.find({},function(err, users){
      res.render('home', {
        title:'Ogłoszenia',
        articles: articles,
        data:users,
        moment:moment
      });
    });
  });
});



// Express Validator
router.use(expressValidator({
  errorFormatter: function(param, msg, value) {
      var namespace = param.split('.')
      , root    = namespace.shift()
      , formParam = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));

// get article
router.get('/article/:id', function(req,res){
  //check who is logged in
  var logged;
  if(req.user){
    logged = req.user._id;
  }else{
    logged = 0;
  };
try {
  Article.findById(req.params.id, function(err,article){
    try {User.findById(article.author, function(err,usersData){
      console.log(article.dates.created);
      res.render('article', {
        article:article,
        data:usersData,
        moment:moment,
        author: usersData.name,
        logged:logged
      });
    });
  }catch(e) {
        res.redirect('/home');
        req.flash('error', 'coś poszło nie tak');
        console.log('????');
      }

  });
} catch(e) {
      res.redirect('/home');
      req.flash('error', 'coś poszło nie tak');
      console.log('????');
    }
});

router.get('/edit/:id', ensureAuthenticated, function(req,res){
  Article.findById(req.params.id, function(err,article){
    res.render('edit', {article:article});
  });
});

//Add Sybmit
router.post('/add',ensureAuthenticated, function(req,res){
  req.checkBody('title', 'Podaj Tytuł').notEmpty();
  req.checkBody('body', 'Nie ma treści').notEmpty();

  let errors = req.validationErrors();
  if(errors){
    res.render('add', {
      errors:errors
    })
  } else {
    let article = new Article({
      title: req.body.title,
      body: req.body.body,
      author:req.user._id,
    });

    article.save(function(err){
      if(err) {
        res.render('add');
      } else {
        req.flash('success', 'Artykuł został dodany')
        res.redirect('/home');
      }
    });

  }

});

// DELETE ARTICLE
router.get('/delete/:id', ensureAuthenticated, function(req,res){
  //check who is logged in
  var logged;
  if(req.user){
    logged = req.user._id;
  }else{
    logged = 0;
  };
if(logged == req.params.id){
  let query = {_id:req.params.id}
  Article.remove(query, function(err){
      if(err){
        console.log(err);
      } else {
        req.flash('success', 'Artykuł usunięto');
      }
    });
  } else {
  req.logout();
  req.flash('nice try');
  res.redirect('/');
}

});


// update submit
router.post('/edit/:id', ensureAuthenticated, function(req,res){
  let article = {
    title: req.body.title,
    author:req.body.author,
    body: req.body.body
  };

  let query = {_id:req.params.id};

  Article.update(query, article,function(err){
    if(err){
      res.render('edit');
    }else {
      req.flash('success', "Artykuł poprawiony");
      res.redirect('/');
    }
  });
});

function ensureAuthenticated(req,res,next){
  if(req.isAuthenticated()) {
    return next();
  } else {
    req.flash('danger', 'Zaloguj się');
    res.redirect('/users/login')
  }
}


// USER METHODS

router.post('/profile/edit/:id', ensureAuthenticated, function(req,res){
  let article = {
    name: req.body.name,
    about:req.body.about,
    education:req.body.education,
  };

  let query = {_id:req.params.id};

  User.update(query, article,function(err){
    if(err){
      res.render('edit');
    }else {
      req.flash('success', "Dane Zaktualizowane");
      res.redirect('/profile/' + req.user._id);
    }
  });
});
router.get('/profile/:id', function(req,res){
    User.findById(req.params.id, function(err,user){
      var photo =   user.photo ;
      var profile_pic =  user.profile_pic;
      res.render('profile', {
        users:user,
        data: req.user,
        photo:photo,
        profile_pic:profile_pic
      });
    });
  });


router.get('/profile/edit/:id', ensureAuthenticated, function(req,res){
  User.findById(req.params.id, function(err,user){
    var photo =  user.photo ;
    var profile_pic =  user.profile_pic;
    res.render('profile_edit', {
      users:user,
      data: req.user,
      photo:photo,
      profile_pic:profile_pic
    });
  });
});

// UPLOAD PROFILE PIC
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'upload/')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
  }
});
var uploadimg = multer({
   storage: storage
 }).single('image');

router.post('/profile/upload/:id', function (req,res) {
  let user_id = req.params.id;
  uploadimg(req, res, function (err) {
    if (err) {console.log(err)}
    else{
      fs.unlink('upload/' + req.user.photo , function(err, err){
              if(err){
                console.log('error deleting');
                User.update(query, photo, function(){});
                res.redirect('/profile/edit/' + user_id);
              } else {
                console.log('successfully deleted users photo');
                filename = req.file.filename;
                let photo = {
                    photo: filename
                  };
                    let query = {_id:user_id};
                    User.update(query, photo, function(){});
                    req.flash('success', 'Zdjęcie Przesłane');
                    res.redirect('/profile/edit/' + user_id);

              }
            });

      }
    });
});

// Upload profile image
    var uploadprofile = multer({
       storage: storage
     }).single('profile');

    router.post('/profile/upload_profile/:id', function (req,res) {
      let user_id = req.params.id;
      uploadprofile(req, res, function (err) {
        if (err) {console.log(err)}
        else{
          fs.unlink('upload/' + req.user.profile_pic , function(err, err){
                  if(err){
                    console.log('error deleting');
                    User.findByIdAndUpdate(user_id, {
                      $set: {
                        'profile_pic':'images/profile_pic.jpg',
                      }, function(){}});
                    res.redirect('/profile/edit/' + user_id);
                  } else {
                    console.log('successfully deleted users photo');
                    filename = "/upload/" + req.file.filename;
                    User.findByIdAndUpdate(user_id, {
                      $set: {
                        'profile_pic':filename,
                      }
                    }, function(){});
                        req.flash('success', 'Zdjęcie Przesłane');
                        res.redirect('/profile/edit/' + user_id);

                  }
                });

          }
        });
      });


// SEARCH
router.post('/search', function(req, res){
var term = "\"" + req.body.search + "\"";
console.log(term);
// let articles = Article.find({$text: {$search:term}}, {score: {$meta: "textScore"}}).sort({score:{$meta:"textScore"}});
// let users = User.find({$text: {$search:term}}, {score: {$meta: "toextScore"}}).sort({score:{$meta:"textScore"}});
 User.find({$text: {$search:term}}, function(err, users){
   Article.find({$text:{$search: term}}, function(err, articles){
     res.render('home', {
       data:users,
       articles:articles,
       moment:moment
     });
   });
 });
    // res.render('home', {
    //   articles: articles,
    //   data:users
    // });
  });

module.exports = router;
