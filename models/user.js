const mongoose = require('mongoose');

// User Schema
const UserSchema = mongoose.Schema({
name:{
      type: String,
      required:true
},
username:{
    type:String,
    required:true
},
email:{
    type:String,
    required:true,
    // unique:true
},
password:{
    type:String,
    required:true
},
isVerified: { type: Boolean, default: false },
token: {type:String},
profile_pic: {
  type:String,
  default:'/images/profile_pic.jpg'
},
about: {
  type:String
},
conversation:[{
  sender:  mongoose.Schema.Types.ObjectId,
  author : String,
  last: String
}
  ],
instrument:{
  type:String,

},
photo: {
  type:String,
  default:'/images/profile.png'
},
education:{
  type:String,

}
// education:[{
//         school:{
//           type:String
//         },
//         completed:{
//           type:String
//         },
//         description:{
//           type:String
//         }
// }]
});
UserSchema.index({about: 'text',
                  name: 'text'
}, function(err, data){
  if(err)
     console.log(err);
    //  console.log(data);
    });


const User = module.exports = mongoose.model('User', UserSchema);
