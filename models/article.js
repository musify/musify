let mongoose = require('mongoose');

// article Schema

let articleSchema = mongoose.Schema({
  title:{
    type:String,
    required:true
  },

  author:{
    type:String,
    required:true
  },

  body: {
    type: String,
    required:true
  },
  dates: {
    created:  {type: Date, default: Date.now},
}


});
articleSchema.index({body: 'text',
                    title: 'text'
  }, function(err, data){
    if(err)
       console.log(err);
      //  console.log(data);
      });

let Article = module.exports = mongoose.model('Article', articleSchema);
