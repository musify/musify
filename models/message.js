let mongoose = require('mongoose');
var messageSchema = mongoose.Schema({
    person : [
        {type : mongoose.Schema.Types.ObjectId}
    ],
    messages : [
        {
            message : String,
            author : String,
            date:{type: Date, default: Date.now}

        }
    ]
    // is_group_message : { type : Boolean, default : false },
    // participants : [
    //     {
    //         user :  {
    //             type : mongoose.Schema.Types.ObjectId,
    //             ref : 'User'
    //         },
    //         delivered : Boolean,
    //         read : Boolean,
    //         last_seen : Date
    //     }
    // ]
});

let Message = module.exports = mongoose.model('Message', messageSchema);
