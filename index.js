const express  = require('express');
var favicon = require('serve-favicon');
const bodyParser = require('body-parser');
const app = express();
app.use(favicon(__dirname + '/public/images/favicon.ico'));
const path = require('path');
const mongoose = require('mongoose');
const expressValidator = require('express-validator');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');
const config = require('./config/database');
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
module.exports = io;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Express Session Middleware
app.use(session({
  secret: 'keyboard cat',
  resave: true,
  saveUninitialized: true
}));

// Express Messages Middleware
app.use(require('connect-flash')());
app.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});

mongoose.connect(config.database);
let db = mongoose.connection;
app.use(express.static(path.join(__dirname,'public')));
app.use('/ui', express.static(__dirname + '/node_modules/materialize-css/'));
app.use('/images', express.static(__dirname + '/public/images/'));
app.use('/upload', express.static(__dirname + '/upload/'));

db.once('open', function(){
  console.log('conncected to db');
})
// DB ERRORS
db.on('error', function(err){
  console.log(err);

});

let Article =require('./models/article');


// PASSPORT CONFIG
require('./config/passport')(passport);
// Passport Middleware
app.use(passport.initialize());
app.use(passport.session());
app.get('*', function(req,res, next){
  res.locals.user = req.user|| null;
  next();
});
// HOME ROUTE
app.get('/', function(req, res){
  Article.find({}, function(err, articles){
    res.render('index', {
      title:'Articles',
      articles: articles
    });
  });
});


// ROUTE FILES
let home = require('./routes/articles');
let users = require('./routes/users');
let messages = require('./routes/messages');
app.use('/users', users);
app.use('/', home);
app.use('/inbox', messages);

// load view engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// START server
server.listen(3000);
//socket
